# -*- coding: utf-8 -*-

import pygame, sys
from src.conf import WINDOW_WIDTH, WINDOW_HEIGHT

class GameManager(object):
    def __init__(self):
        self.screen = pygame.display.set_mode((WINDOW_WIDTH, WINDOW_HEIGHT))
        self.stack = []
        self.stack_size = 0

        self.clock = pygame.time.Clock()

        self.scene_exit = False

    def run(self):
        while self.stack_size > 0:
            scene = self.stack[self.stack_size - 1]
            self.loop(scene)

    def loop(self, scene):
        self.scene_exit = False

        pygame.event.clear()

        while not self.scene_exit:
            events = pygame.event.get()

            for event in events:
                if event.type == pygame.QUIT or (event.type == pygame.KEYDOWN and event.key == pygame.K_ESCAPE):
                    sys.exit()

            delta_time = self.clock.tick(60) / 1000

            scene.events(events)
            scene.update(delta_time)

            self.screen.fill((0, 0, 0))
            scene.draw(self.screen)
            pygame.display.flip()

    def scene_pop(self):
        self.scene_exit = True

        if (self.stack_size > 0):
            self.stack.pop()
            self.stack_size -= 1

    def program_exit(self):
        self.scene_exit = True
        self.stack = []
        self.stack_size -= 1

    def scene_change(self, scene):
        self.scene_pop()
        self.stack.append(screen)

    def scene_push(self, scene):
        self.scene_exit = True
        self.stack.append(scene)
        self.stack_size += 1
        