# -*- encoding: utf-8 -*-

import pygame
from src.ui.uielement import UIElement

class UIScreen(UIElement):
    def __init__(self, stage):
        self.elements = []
        self.stage = stage

    def add_element(self, element):
        self.elements.append(element)

    def remove_element(self, element):
        self.elements.remove(element)

    def update(self, delta_time):
        for element in self.elements:
            element.update(delta_time)

    def draw(self, screen):
        for element in self.elements:
            element.draw(screen)