# -*- encoding: utf-8 -*-

import pygame, random
from src.resourcemanager import ResourceManager
from src.ui.scenes.uiscreen import UIScreen
from src.ui.uiitem import UIItem
from src.conf import *

MAX_TIME = 0.5
MAX_BEANS = 10

class UICoffeeGame(UIScreen):
    def __init__(self, stage):
        UIScreen.__init__(self, stage)

        self.delta_time = 0
        self.current_beans = 0
        self.add_element(UIItem("bean"))

    def update(self, delta_time):
        UIScreen.update(self, delta_time)

        self.delta_time += delta_time
        if self.delta_time >= MAX_TIME:
            self.delta_time -= MAX_TIME

            item_type = random.randint(0, 1)
            if item_type == 0:
                self.add_element(UIItem("bean"))
            else:
                self.add_element(UIItem("leaf"))

        killed_elements = []
        for element in self.elements:
            if element.life >= element.max_life:
                killed_elements.append(element)

        for element in killed_elements:
            self.remove_element(element)

    def mouse_pressed(self, position):
        if position[0] < WINDOW_WIDTH / 2 + 1:
            pass
        
        pressed_element = None
        for element in self.elements:
            if element.rect.collidepoint(position) and element.item == "bean":
                pressed_element = element
                self.current_beans += 1
                break

        if pressed_element is not None:
            self.remove_element(pressed_element)

        if self.current_beans == MAX_BEANS:
            self.stage.beans_complete()

    def draw(self, screen):
        UIScreen.draw(self, screen)

        rect_width = WINDOW_WIDTH / 2 - 40
        beans_width = self.current_beans * rect_width / MAX_BEANS

        pygame.draw.rect(screen, (255, 0, 0), pygame.Rect(WINDOW_WIDTH / 2 + 20, 20, beans_width, 20))
        pygame.draw.rect(screen, (255, 255, 255), pygame.Rect(WINDOW_WIDTH / 2 + 20, 20, rect_width, 20), 2)