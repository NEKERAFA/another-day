# -*- encoding: utf-8 -*-

import pygame
from src.ui.uitext import UIText
from src.ui.scenes.uiscreen import UIScreen
from src.resourcemanager import ResourceManager
from src.conf import *

class UIMainMenu(UIScreen):
    def __init__(self, stage):
        UIScreen.__init__(self, stage)

        big_font = ResourceManager.load_font(DEFAULT_FONT, BIG_FONT_SIZE)
        font = ResourceManager.load_font(DEFAULT_FONT, FONT_SIZE)

        self.title = UIText((WINDOW_WIDTH / 2, WINDOW_HEIGHT / 2 - BIG_FONT_SIZE), big_font, '~ Another day ~', 'center')
        self.add_element(self.title)

        self.subtitle = UIText((WINDOW_WIDTH / 2, WINDOW_HEIGHT / 2 + 4), font, 'in the café', 'center')
        self.add_element(self.subtitle)

        self.start = UIText((WINDOW_WIDTH / 2, WINDOW_HEIGHT - FONT_SIZE - 8), font, 'Press [SPACE] to serve a coffee', 'center')
        self.add_element(self.start)