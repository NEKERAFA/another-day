# -*- encoding: utf-8 -*-

import pygame
from src.ui.scenes.uiscreen import UIScreen
from src.conf import *
from src.ui.uitext import UIText
from src.resourcemanager import ResourceManager

DEFAULT_TEXT_SPEED = 0.05
LINE_SPACE = 2

class UIStage(UIScreen):
    def __init__(self, stage, dialog=[]):
        UIScreen.__init__(self, stage)

        self.font = ResourceManager.load_font(DEFAULT_FONT, FONT_SIZE)

        self.dialog = dialog
        self.intervetion = 0
        self.character = 0
        self.delta_time = 0
        self.surfaces = []
        self.moving_text = False
        self.delta_y = 0

        first_intervetion = self.dialog[0]
        
        if first_intervetion['type'] != 'dialog':
            self.updating_text = False
            self.stage.check_intervention(first_intervetion)
        else:
            self.updating_text = True
            self.surfaces.append(UIText((0, 0), self.font, '', color=first_intervetion['color']))
            self.current_surface = self.surfaces[0]
            self.add_element(self.surfaces[0])

    def update(self, delta_time):
        UIScreen.update(self, delta_time)
        
        if self.updating_text:
            self.delta_time += delta_time
            if self.delta_time >= DEFAULT_TEXT_SPEED:
                self.delta_time -= DEFAULT_TEXT_SPEED
                self.update_character()

        if self.moving_text:
            delta_y = delta_time * (FONT_SIZE + LINE_SPACE) * 64
            self.delta_y -= delta_y
            if self.delta_y <= - (FONT_SIZE + LINE_SPACE):
                self.delete_first_surface()
            else:
                self.move_surfaces(delta_y)
                    
    def update_character(self):
        current_intervention = self.dialog[self.intervetion]

        self.character += 1
        if self.character == len(current_intervention['text']) + 1:
            self.move_intervetion(self.intervetion + 1)
        else:
            self.current_surface.change_text(current_intervention['text'][0:self.character])

    def next_intervention(self):
        self.updating_text = True
        self.move_intervetion(self.intervetion + 1)

    def change_intervetion(self, intervetion):
        self.updating_text = True
        self.move_intervetion(intervetion)

    def move_intervetion(self, intervetion):
        if intervetion >= len(self.dialog):
            self.updating_text = False
            self.stage.finish_dialog()
        else:
            self.character = 0
            self.intervetion = intervetion

            next_intervention = self.dialog[self.intervetion]
            if next_intervention['type'] != 'dialog':
                self.updating_text = False
                self.stage.check_intervention(next_intervention)
            else:
                y_pos = self.current_surface.rect.top + FONT_SIZE + LINE_SPACE
                if y_pos > WINDOW_HEIGHT - FONT_SIZE - LINE_SPACE:
                    self.moving_text = True

                new_surface = UIText((0, y_pos), self.font, '', color=next_intervention['color'])
                
                self.surfaces.append(new_surface)
                self.add_element(new_surface)
                self.current_surface = new_surface

    def move_surfaces(self, delta_y):
        for uitext in self.surfaces:
            new_y = uitext.rect.top - delta_y
            uitext.set_position((uitext.rect.left, new_y))

    def delete_first_surface(self):
        self.moving_text = False
        self.delta_y = 0
        first_intervetion = self.surfaces.pop(0)
        self.remove_element(first_intervetion)
        del first_intervetion

        delta_y = 0
        for uitext in self.surfaces:
            uitext.set_position((uitext.rect.left, delta_y))
            delta_y += FONT_SIZE + LINE_SPACE
