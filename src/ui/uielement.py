# -*- coding: utf-8 -*-

import pygame

class UIElement(object):
    def __init__(self, rect):
        self.rect = rect

    def update(self, delta_time):
        raise NotImplemented("You must implement update method")

    def draw(self, screen):
        raise NotImplemented("You must implement draw method")