# -*- coding: utf-8 -*-

import pygame
from src.ui.uielement import UIElement
from src.resourcemanager import ResourceManager

class UISprite(UIElement, pygame.sprite.Sprite):
    def __init__(self, path, size):
        pygame.sprite.Sprite.__init__(self)

        image = ResourceManager.load_image(path)
        self.image = pygame.transform.scale(image, size)

        self.rect = self.image.get_rect()

    def update(self, delta_time):
        pygame.sprite.Sprite.update(self, delta_time)

    def draw(self, screen):
        screen.blit(self.image, self.rect)