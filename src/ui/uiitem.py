# -*- coding: utf-8 -*-

import pygame, random
from src.ui.uisprite import UISprite
from src.conf import *

class UIItem(UISprite):
    def __init__(self, item):
        path = "bean.png" if item == "bean" else "leaf.png"
        UISprite.__init__(self, path, (32, 32))

        self.image = self.image.convert_alpha()

        self.item = item
        self.life = 0
        self.max_life = random.randint(2, 5)
        self.x = float(random.randint(WINDOW_WIDTH / 2 + 1, WINDOW_WIDTH - self.image.get_width()))
        self.y = float(random.randint(0, WINDOW_HEIGHT - self.image.get_height()))
        self.rect.topleft = (int(self.x), int(self.y))
        self.direction = (random.randint(-128, 128), random.randint(-128, 128))

    def update(self, delta_time):
        UISprite.update(self, delta_time)

        self.life += delta_time
        self.x += self.direction[0] * delta_time
        self.y += self.direction[1] * delta_time
        self.rect.topleft = (int(self.x), int(self.y))

        self.image.set_alpha(255 - self.life * 255 / self.max_life)
