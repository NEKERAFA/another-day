# -*- encoding: utf-8 -*-

import pygame
from src.ui.uielement import UIElement

class UIText(UIElement):
    def __init__(self, position, font, text='', alignment='left', color=(255, 255, 255)):
        self.font = font
        self.text = text
        self.color = color
        self.position = position
        self.alignment = alignment
        self.surface = self.font.render(self.text, False, self.color)
        UIElement.__init__(self, self.surface.get_rect())
        self.update_surface()

    def update(self, delta_time):
        pass

    def draw(self, screen):
        screen.blit(self.surface, self.rect)

    def set_position(self, position):
        self.position = position
        self.update_position()

    def change_font(self, font):
        self.font = font
        self.update_surface()

    def change_text(self, text):
        self.text = text
        self.update_surface()

    def change_color(self, color):
        self.color = color
        self.update_surface()

    def change_alignment(self, alignment):
        self.alignment = alignment
        self.update_position()

    def update_surface(self):
        self.surface = self.font.render(self.text, False, self.color)
        self.rect = self.surface.get_rect()
        self.update_position()

    def update_position(self):
        if self.alignment == 'left':
            self.rect.topleft = self.position
        elif self.alignment == 'center':
            self.rect.midtop = self.position
        elif self.alignment == 'right':
            self.rect.topright = self.position