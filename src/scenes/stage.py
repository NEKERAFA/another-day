# -*- encoding: utf-8 -*-

import pygame
from src.scenes.scene import Scene
from src.ui.scenes.uistage import UIStage
from src.ui.scenes.uicoffeegame import UICoffeeGame
from src.resourcemanager import ResourceManager
from src.conf import WINDOW_WIDTH, WINDOW_HEIGHT

class Stage(Scene):
    def __init__(self, game_manager, dialog_file):
        Scene.__init__(self, game_manager)

        dialog = ResourceManager.load_dialogue(dialog_file)
        
        self.ui = UIStage(self, dialog)
        self.game = None
        self.exit = False

    def update(self, delta_time):
        self.ui.update(delta_time)
        if self.game is not None:
            self.game.update(delta_time)

    def events(self, events):
        for event in events:
            if event.type == pygame.KEYDOWN and event.key == pygame.K_SPACE and self.exit:
                self.game_manager.scene_pop()

            if event.type == pygame.MOUSEBUTTONUP:
                if self.game is not None:
                    pos = pygame.mouse.get_pos()
                    self.game.mouse_pressed(pos)

    def finish_dialog(self):
        print('End dialog')
        #self.game_manager.scene_exit()

    def check_intervention(self, intervetion):
        print('Intervention!', intervetion)

        if intervetion['type'] == 'coffee':
            self.game = UICoffeeGame(self)
            self.ui.next_intervention()

        if intervetion['type'] == 'water':
            self.exit = True

    def beans_complete(self):
        self.ui.next_intervention()
        self.game = None

    def draw(self, screen):
        if self.game is not None:
            self.game.draw(screen)

        pygame.draw.rect(screen, (0, 0, 0), pygame.Rect(0, 0, WINDOW_WIDTH / 2, WINDOW_HEIGHT))

        pygame.draw.line(screen, (255, 255, 255), (WINDOW_WIDTH / 2, 0), (WINDOW_WIDTH / 2, WINDOW_HEIGHT))

        self.ui.draw(screen)