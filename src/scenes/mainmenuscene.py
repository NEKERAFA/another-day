# -*- encoding: utf-8 -*-

import pygame
from src.gamemanager import GameManager
from src.scenes.scene import Scene
from src.ui.scenes.uimainmenu import UIMainMenu
from src.scenes.stage import Stage

class MainMenuScene(Scene):
    def __init__(self, game_manager):
        Scene.__init__(self, game_manager)

        self.ui = UIMainMenu(self)

    def update(self, delta_time):
        self.ui.update(delta_time)

    def events(self, events):
        for event in events:
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_SPACE:
                    stage1 = Stage(self.game_manager, "stage1.json")
                    self.game_manager.scene_push(stage1)

    def draw(self, screen):
        self.ui.draw(screen)