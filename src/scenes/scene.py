# -*- encoding: utf-8 -*-

class Scene(object):
    def __init__(self, game_manager):
        self.game_manager = game_manager

    def update(self, delta_time):
        raise NotImplemented("You must implement update method")

    def events(self, events):
        raise NotImplemented("You must implement events method")

    def draw(self, screen):
        raise NotImplemented("You must implement draw method")