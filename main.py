#!/usr/bin/env python
# -*- coding: utf-8 -*-

import pygame, random, time, sys, os
from src.gamemanager import GameManager
from src.scenes.mainmenuscene import MainMenuScene

def resource_path(relative_path):
    try:
    # PyInstaller creates a temp folder and stores path in _MEIPASS
        base_path = sys._MEIPASS
    except Exception:
        base_path = os.path.abspath(".")

    return os.path.join(base_path, relative_path)

if __name__ == '__main__':
    pygame.init()
    random.seed(time.time())

    pygame.display.set_caption('~ Another Day ~')
    
    game_manager = GameManager()
    scene = MainMenuScene(game_manager)
    game_manager.scene_push(scene)
    
    game_manager.run()
    
    pygame.quit()